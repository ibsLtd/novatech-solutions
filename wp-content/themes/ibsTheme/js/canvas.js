window.addEventListener('DOMContentLoaded', (event) => {

    const canvas = document.querySelector('canvas')
    const c = canvas.getContext('2d')


    const colors = ['#5C258D', '#4389A2']

    const random = function rand(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min)
    };
    const randomColor = function (colors) {
        return colors[Math.floor(Math.random() * colors.length)]
    }

    let circles = []
    let frame = 500

    canvas.width = innerWidth
    canvas.height = innerHeight

    addEventListener('resize', () => {
        canvas.width = innerWidth
        canvas.height = innerHeight
		init()
    })

    function Circle(x, y, radius, color) {


        this.x = x
        this.y = y
        this.vr = 0;
        this.life = true
        this.color = color
        this.radius = radius
        this.vy = random(.1, .5)
        this.vx = random(-3, 3)


        this.draw = function () {
            c.beginPath()
            c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
            c.fillStyle = this.color
            c.fill()
            c.closePath()
        }

        this.update = function () {

            this.vy += .07
            this.vr += .012
            this.y -= this.vy
            this.x += this.vx

            if (this.radius > 1) this.radius -= this.vr

            if (this.radius <= 1) this.life = false

            if (this.life) this.draw();


        }
    }


    function init() {

        circles.push(new Circle(random(-random(30, 120), window.innerWidth),
            random(window.innerHeight + random(30, 120), window.innerHeight + random(30, 120) + 20),
            random(30, 120), randomColor(colors))
        )
        frame += frame
    }

    function animate() {
        requestAnimationFrame(animate)
        c.clearRect(0, 0, canvas.width, canvas.height)
        if (circles.length < frame) {
            init()
        }
        circles.forEach(circle => {
            circle.update()
        })


    }

    if(document.body.classList.contains('page-template-one-page')){
        init()
        animate()

    }


})
