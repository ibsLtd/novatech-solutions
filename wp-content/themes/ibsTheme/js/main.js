"use strict";
jQuery(document).ready(function ($) {
    const dom = {
        init: function () {

            if (window.location.hash) {
                dom.setClasses(window.location.hash)
                dom.setClasses($("#fp-nav  a[href^=" + window.location.hash + "]").parent())
                dom.setClasses($("#et-footer-nav a[href^=" + window.location.hash + "]").parent())
                $(window.location.hash).scrollTop(0, 0)
            } else {
                $('.et_pb_section').first().addClass('active')
                $('.et_pb_section.active').scrollTop(0, 0)
                $("#et-footer-nav li").first().addClass('active')

            }
            if (window.matchMedia("(min-width:981px)").matches && $('body').hasClass('home')) {
                $('.et_pb_section').height($(window).height())
            }


        },
        setClasses: function (elem) {
            $(elem).addClass('active')
                .siblings().removeClass('active')
        },
        resetClasses: function (elem) {
            const timer = setTimeout(function () {
                $(elem).removeClass('active')
                clearTimeout(timer)
            }, 1500)
        },
        onScroll: function (delta) {
            const active = $('.et_pb_section.active')

            if (delta < 0) {
                const next = active.next(),
                    rightNavigation = $("#fp-nav  a[href^=#" + next.attr('id') + "]").parent(),
                    bottomNavigation = $("#et-footer-nav a[href^=#" + next.attr('id') + "]").parent()
                if (next.length && next.attr('id') !== 'undefined') {
                    window.location.hash = '#' + next.attr('id')
                    const timer = setTimeout(function () {
                        $('body, html').animate({
                            scrollTop: next.offset().top
                        }, 500);
                        dom.setClasses(next)
                        dom.setClasses(bottomNavigation)
                        dom.setClasses(rightNavigation)
                        dom.resetClasses(rightNavigation)
                        clearTimeout(timer)
                    }, 800);

                }

            } else {
                const prev = active.prev(),
                    rightNavigation = $("#fp-nav  a[href^=#" + prev.attr('id') + "]").parent(),
                    bottomNavigation = $("#et-footer-nav a[href^=#" + prev.attr('id') + "]").parent()


                if (prev.length && prev.attr('id') !== 'undefined') {
                    window.location.hash = '#' + prev.attr('id')
                    const timer = setTimeout(function () {
                        $('body, html').animate({
                            scrollTop: prev.offset().top
                        }, 500)
                        console.log(prev.offset().top)
                        dom.setClasses(prev)
                        dom.setClasses(bottomNavigation)
                        dom.setClasses(rightNavigation)
                        dom.resetClasses(rightNavigation)
                        clearTimeout(timer)
                    }, 800)

                }

            }

        }
    }

    //INIT
    dom.init()

    //EVENTS
    $("#et-footer-nav a[href^='#']").on('click', function () {
        const parent = $(this).parent(),
            section = $(this).attr("href")
        if ($('body').hasClass('home')) {
            dom.setClasses(parent)
            dom.setClasses(section)
            window.location.hash = section
            if (window.matchMedia("(min-width:981px)").matches && $('body').hasClass('home')) {
                const rightNavigation = $("#fp-nav  a[href^=" + section + "]").parent()
                dom.setClasses(rightNavigation)
                dom.resetClasses(rightNavigation)
            }
        } else {
            window.location.href = window.location.origin + section
        }

    })

    $('#works a').on('click', function (e) {
        e.preventDefault()
    })

    if (window.matchMedia("(min-width:981px)").matches && $('body').hasClass('home')) {
        $("#fp-nav  a[href^='#']").on('click', function () {
            const parent = $(this).parent(),
                section = $(this).attr("href"),
                bottomNavigation = $("#et-footer-nav a[href^=" + section + "]").parent()
            window.location.hash = section
            dom.setClasses(parent)
            dom.setClasses(section)
            dom.setClasses(bottomNavigation)
            dom.resetClasses(parent)
        })

        $(document).on('mousewheel DOMMouseScroll', function (e) {
            const delta = e.originalEvent.detail < 0 || e.originalEvent.wheelDelta > 0 ? 1 : -1
            dom.onScroll(delta)
        });


    }
});
