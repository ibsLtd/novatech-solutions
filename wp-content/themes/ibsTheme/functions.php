<?php


function register_scripts()
{

    if (!is_admin()) {
        wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
        //wp_enqueue_style('mainCss', get_stylesheet_directory_uri()  . '/main.css');
        wp_enqueue_script('mainJs', get_stylesheet_directory_uri() . '/js/main.js', array('jquery'),true);
        wp_enqueue_script('canvas', get_stylesheet_directory_uri() . '/js/canvas.js', array(),true);
    }
}

add_action('wp_enqueue_scripts', 'register_scripts');





